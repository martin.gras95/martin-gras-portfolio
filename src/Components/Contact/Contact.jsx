import React from "react";
import { Section } from "../.";
import "./Contact.styles.scss";

const Contact = () => {
	return (
		<Section id={"Contact"} title={"Contact"}>
			<div className="tile is-parent">
				<div className="tile is-child box is-vertical contact-tile ">
					<div className="columns">
						<div className="column direct-contact-column">
							<div className="contact-item">
								<h3 className="is-size-5">
									<span className="icon">
										<ion-icon name="location-sharp"></ion-icon>
									</span>
									Location
								</h3>
								<span>Amsterdam, NL</span>
							</div>
							<div className="contact-item">
								<h3 className="is-size-5">
									<span className="icon">
										<ion-icon name="call-sharp"></ion-icon>
									</span>
									Phone
								</h3>
								<span>+31 633932496</span>
							</div>
							<div className="contact-item">
								<h3 className="is-size-5">
									<span className="icon">
										<ion-icon name="mail-sharp"></ion-icon>
									</span>
									E-mail
								</h3>
								<span>Martin.Gras95@gmail.com</span>
							</div>
						</div>
						<div className="column">
							<div className="contact-item">
								<a
									href="https://www.linkedin.com/in/martingras"
									target={"_blank"}
									rel="noreferrer"
								>
									<h3 className="is-size-5">
										<span className="icon">
											<ion-icon name="logo-linkedin"></ion-icon>
										</span>
										LinkedIn
									</h3>
								</a>
							</div>
							<div className="contact-item">
								<a
									href="https://gitlab.com/martin.gras95"
									target={"_blank"}
									rel="noreferrer"
								>
									<h3 className="is-size-5">
										<span className="icon">
											<ion-icon name="logo-gitlab"></ion-icon>
										</span>
										GitLab
									</h3>
								</a>
							</div>
							<div className="contact-item">
								<a
									href="https://github.com/MartinGras95"
									target={"_blank"}
									rel="noreferrer"
								>
									<h3 className="is-size-5">
										<span className="icon">
											<ion-icon name="logo-github"></ion-icon>
										</span>
										Github
									</h3>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Section>
	);
};

export default Contact;
