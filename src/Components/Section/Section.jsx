import React from "react";
import "./Section.styles.scss";

const Section = ({ children, title, id, hasBottomBorder }) => {
	return (
		<section className={`section is-medium ${hasBottomBorder ? 'section--has-bottom-border': ''}`}>
			<div className="container" id={id}>
				<h1 className="title is-size-1-desktop is-size-3-touch">{title}</h1>
				<div className="tile is-ancestor section-box">{children}</div>
			</div>
		</section>
	);
};

export default Section;
