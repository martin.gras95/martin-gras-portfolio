import React from "react";
import "./navbar.scss";

const Navbar = () => {
	return (
		<nav className="navbar is-fixed-top" role="navigation" aria-label="main navigation">
			<div  className="navbar-menu">
				<div className="navbar-items">
					<a href="#Skills"  className="navbar-item">
						Skills
					</a>

					<a href="#Contact" className="navbar-item">
						Contact
					</a>
				</div>
			</div>
		</nav>
	);
};

export default Navbar;
