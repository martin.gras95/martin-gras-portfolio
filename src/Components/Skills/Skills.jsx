import React from "react";
import Section from "../Section/Section";
import "./Skills.styles.scss";

const Skills = () => {
	return (
		<Section title={"Skills"} id={"Skills"} hasBottomBorder={true}>
			<div className="tile is-parent">
				<div className="tile is-child box is-vertical skill-tile">
					<h2 className="is-size-4">Front-end</h2>
					<span className="icon is-large ">
						<ion-icon name="code-slash-sharp"></ion-icon>
					</span>
					<p>
						I enjoy bringing designs to life, making them accessible (WCAG) and
						seamless to use. JavaScript and React.js are my bread and butter.
					</p>
					<h3 className="is-size-5">Languages</h3>
					<ul>
						<li>JavaScript (ES6+)</li>
						<li>TypeScript</li>
						<li>HTML5</li>
						<li>CSS / SCSS</li>
					</ul>
					<h3 className="is-size-5">Tech stack</h3>
					<ul>
						<li>React.js</li>
						<li>Redux</li>
						<li>Jest</li>
						<li>Web components</li>				
					</ul>
					<h3 className="is-size-5">Favorite editor</h3>
					<ul>
						<li>Visual Studio Code</li>
					</ul>
				</div>
			</div>
			<div className="tile is-parent">
				<div className="tile is-child box is-vertical skill-tile">
					<h2 className="is-size-4">Collaboration</h2>
					<span className="icon is-large ">
						<ion-icon name="people-circle-sharp"></ion-icon>{" "}
					</span>
					<p>
						I'm cool with collaborating with my teammates using all sorts of
						strategies and tools! You can also often catch me at the coffee
						machine.
					</p>
					<h3 className="is-size-5">Design</h3>
					<ul>
						<li>Figma</li>
						<li>Lunacy</li>
						<li>Storybook</li>
						<li>Atomic design</li>
						<li>Accessibility</li>
					</ul>
					<h3 className="is-size-5">Agile</h3>
					<ul>
						<li>Agile rituals</li>
						<li>Azure Devops</li>
						<li>Gitlab</li>
					</ul>
				</div>
			</div>
		</Section>
	);
};

export default Skills;
