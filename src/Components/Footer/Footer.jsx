import React from 'react'
import './Footer.scss'

const Footer = () => {
  return (
    <footer className='footer'>
        <div className='content has-text-centered'>
            <span>&copy; Martin Gras 2022</span>
        </div>
    </footer>
  )
}

export default Footer