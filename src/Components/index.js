export { default as Skills } from "./Skills";
export { default as Hero } from "./Hero";
export { default as Section } from "./Section";
export { default as Navbar } from "./Navbar";
export { default as Contact } from "./Contact";
export { default as Footer } from "./Footer";
