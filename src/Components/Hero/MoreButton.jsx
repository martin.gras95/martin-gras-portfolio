import React from "react";

const MoreButton = () => {
	return (
		<a className="button more-button" href="#Skills">
			Check out my skillset!
		</a>
	);
};

export default MoreButton;
