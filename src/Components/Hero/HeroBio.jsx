import React from "react";
import profiler from "./profile.png";

const HeroBio = () => {
	return (
		<div className="columns hero-bio">
			<figure className="image is-256x256">
				<img src={profiler} alt="Pixelated Martin" />
			</figure>
			<p>
				Front-end developer specializing in React.js. Striving to make the web a
				seamless experience for every user.
			</p>
		</div>
	);
};

export default HeroBio;
