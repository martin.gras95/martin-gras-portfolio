import React from "react";
import "./hero.scss";
import HeroBio from "./HeroBio";
import ParticlesBg from "particles-bg";
import MoreButton from "./MoreButton";

const Hero = () => {
	return (
		<section className="hero is-fullheight">
			<div className="hero-body container">
				{/* Title & subtitle */}
				<div className="section">
					<p className="title">
						Martin <span className="lime">Gras</span>
					</p>
					<p className="subtitle">- Front-end Developer</p>
				</div>
				{/* Profile picture & bio */}
				<div className="section bio-section">
					<HeroBio />
				</div>
				<div className="section">
					<MoreButton />
				</div>
			</div>
			<ParticlesBg num={25} type="square" bg={true} />
		</section>
	);
};

export default Hero;
