import "bulma/css/bulma.min.css";
import "./app.scss";
import { Skills, Navbar, Hero, Contact, Footer } from "./Components";

const App = () => {
	return (
		<div className="App">
			<Navbar />
			<Hero />
			<Skills />
			<Contact />
			<Footer />
		</div>
	);
};

export default App;
